package at.ibm.codechallenge.persistence.json;

import at.ibm.codechallenge.domainobjects.util.json.JsonService;
import at.ibm.codechallenge.persistence.Repository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Abstract class for repositories, which load and keep their data in json files.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public abstract class JsonRepository<T> implements Repository<T,String> {

    /**
     * Json jsonService for initialising the data
     */
    private JsonService<T[]> jsonService;

    /**
     * JsonString, containing the data for the repository
     */
    private String jsonString;

    /**
     * Map for keeping the data, after it was loaded by the jsonService
     */
    private Map<String,T> objects = new HashMap<String, T>();

    /**
     * Loads the objects from the given json service and fills a list with them.
     *
     * @return  true = successfully loaded the objects, false = couldn't load objects
     */
    public boolean initialiseObjects(){
        try {
            T[] objectArray = jsonService.readObjectFromJsonString(jsonString);
            //fill the map with the objects and random IDs
            for(T object : objectArray){
                objects.put(UUID.randomUUID().toString(),object);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Map<String,T> getObjects() {
        return objects;
    }

    public void setJsonService(JsonService<T[]> jsonService) {
        this.jsonService = jsonService;
    }

    public JsonService<T[]> getJsonService() {
        return jsonService;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }
}

