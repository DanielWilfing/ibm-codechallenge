package at.ibm.codechallenge.persistence.json;

import at.ibm.codechallenge.domainobjects.Movie;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * Json Repository containing Movie objects.
 * Implements the required functionality, defined by the Repository interface.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public class MovieJsonRepository extends JsonRepository<Movie> {

    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(MovieJsonRepository.class);

    /**
     * Flag to check, if the data of the repository has been loaded.
     */
    private boolean initialised = false;

    public boolean save(String s, Movie object) {
        if(!checkInitialise()){
            return false;
        }

        String id;
        if(s != null){
            //check if there already is a movie with the given id
            //if yes, then we can't save this object
            if(getObjects().containsKey(s)){
                return false;
            }

            id = s;
        }
        else {
            //otherwise just set a new random ID
            id = UUID.randomUUID().toString();
        }

        object.setId(id);
        getObjects().put(id,object);

        return false;
    }

    public boolean delete(String s) {
        if(!checkInitialise()){
            return false;
        }

        if(!getObjects().containsKey(s)){
            logger.warn("Could not remove the movie, as the given id wasn't found!");
            return false;
        }

        getObjects().remove(s);

        return true;
    }

    public boolean update(String s, Movie object) {
        if(!checkInitialise()){
            return false;
        }

        if(!getObjects().containsKey(s)){
            logger.warn("Could not update the movie, as the given id wasn't found!");
            return false;
        }

        object.setId(s);
        getObjects().put(s,object);

        return true;
    }

    public boolean exists(String s) {
        if(!checkInitialise()){
            return false;
        }

        return getObjects().containsKey(s);
    }

    public Movie get(String s) {
        if(!checkInitialise()){
            return null;
        }

        return getObjects().get(s);
    }

    public Collection<Movie> getAll() {
        if(!checkInitialise()){
            return null;
        }

        return getObjects().values();
    }

    /**
     * Checks if the repository has been initialised.
     * If not, will initialise the object.
     *
     * @return  true = initialisation successful or already initialised, false = failed to initialise
     */
    private boolean checkInitialise(){
        //check if we already initialised the list of objects
        if(!initialised){
            //if not, initialise it
            if(initialiseObjects()){
                //set the id of the movies
                for(Map.Entry<String,Movie> entry : getObjects().entrySet()){
                    entry.getValue().setId(entry.getKey());
                }
                initialised = true;
            }
            else {
                logger.warn("Could not initialise the movie repository!");
                return false;
            }
        }
        return true;
    }
}
