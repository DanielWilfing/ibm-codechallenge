package at.ibm.codechallenge.persistence;

import java.util.Collection;

/**
 * Interface class for repositories.
 * Allows use of different kinds of repositories, storing different objects
 *
 * @author Daniel Wilfing - 23.9.2017.
 */
public interface Repository<T, ID> {

    /**
     * Save the object with the given id in the repository.
     *
     * @param id        id to save the object with, if no id is given, create a new one
     * @param object    object to save
     * @return          true = successfully saved new object, false = failed to save
     */
    boolean save(ID id, T object);

    /**
     * Delete object with the given id from the repository.
     *
     * @param id        id of the object to delete
     * @return          true = successfully deleted object, false = failed to delete
     */
    boolean delete(ID id);

    /**
     * Update the object with the given id in the repository.
     *
     * @param id        id of the object to update
     * @param object    new object, which will replace the old one
     * @return          true = successfully updated object, false = failed to update
     */
    boolean update(ID id, T object);

    /**
     * Check if an object with the given id exists.
     *
     * @param id        id to check
     * @return          true = object with this exists, false = couldn't find object with this id
     */
    boolean exists(ID id);

    /**
     * Finds the object with the given id.
     * If no object found, returns null.
     *
     * @param id        id of the object to find
     * @return          object with the given id, null if no object with this id found
     */
    T get(ID id);

    /**
     * Get all objects in the repository.
     *
     * @return          Collection containing all objects currently in the repository.
     */
    Collection<T> getAll();

}
