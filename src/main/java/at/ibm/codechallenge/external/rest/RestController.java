package at.ibm.codechallenge.external.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * REST-Interface, defining required methods to use REST together with other objects.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public interface RestController<DTO,ID> {

    /**
     * GET-Method for reading objects using an id.
     *
     * @param id    id of the object to load
     * @return      DTO if the object
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    DTO read(@PathVariable("id") ID id);

    /**
     * GET-Method for reading all objects
     *
     * @return      Collection of DTOs
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    Collection<DTO> read();

    /**
     * POST-Method to save a new object.
     *
     * @param item  object to save
     * @return      Response of the POST-Method
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<String> save(@RequestBody DTO item);

    /**
     * PUT-Method to update an object.
     *
     * @param id    id of the object to update
     * @param item  new object replacing the previous one
     * @return      Response of the PUT-Method
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    ResponseEntity<Void> update(@PathVariable("id") ID id, @RequestBody DTO item);

    /**
     * DELETE-Method to remove the object with the given id
     *
     * @param id    id of the object to delete
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    void delete(@PathVariable("id") ID id);

}
