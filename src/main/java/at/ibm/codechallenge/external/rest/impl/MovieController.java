package at.ibm.codechallenge.external.rest.impl;

import at.ibm.codechallenge.domainobjects.Movie;
import at.ibm.codechallenge.domainobjects.dto.MovieDTO;
import at.ibm.codechallenge.domainobjects.transformer.Transformer;
import at.ibm.codechallenge.external.rest.RestController;
import at.ibm.codechallenge.service.MovieService;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Daniel Wilfing - 24.9.2017.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/movie")
public class MovieController implements RestController<MovieDTO,String> {

    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(RestController.class);

    protected Transformer<Movie, MovieDTO> transformer;

    protected MovieService service;

    public MovieDTO read(@PathVariable("id") String s) {
        Movie object = service.get(s);
        if (object == null) {
            logger.warn("Could not find object with the given id!");
            return null;
        }
        return transformer.to(object);
    }

    public Collection<MovieDTO> read() {
        Collection<MovieDTO> dtos = new ArrayList<MovieDTO>();
        for(Movie m : service.getAll()){
            dtos.add(transformer.to(m));
        }
        return dtos;
    }

    @RequestMapping(value = "filter/{text}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Collection<MovieDTO> filter(@PathVariable("text") String s) {
        Collection<MovieDTO> dtos = new ArrayList<MovieDTO>();
        for(Movie m : service.findByTitlePart(s)){
            dtos.add(transformer.to(m));
        }
        return dtos;
    }

    public ResponseEntity<String> save(@RequestBody MovieDTO item) {
        service.save(transformer.from(item));

        //object we created has to be last in the list
        List<Movie> movies = new ArrayList<Movie>(service.getAll());
        Movie movie = movies.get(movies.size() - 1);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(movie.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    public ResponseEntity<Void> update(@PathVariable("id") String s, @RequestBody MovieDTO item) {
        Movie newT = transformer.from(item);
        newT.setId(s);

        Movie t = service.get(s);
        service.save(newT);
        if (t != null) {
            // we had to update it
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            // we created a new one
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
    }

    public void delete(@PathVariable("id") String s) {
        Movie t = service.get(s);
        if (t == null) {
            logger.warn("Could not delete object with the given id!");
            return;
        }
        service.delete(s);
    }

    public void setTransformer(Transformer<Movie, MovieDTO> transformer) {
        this.transformer = transformer;
    }

    public void setService(MovieService service) {
        this.service = service;
    }

}
