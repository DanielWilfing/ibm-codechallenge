package at.ibm.codechallenge.service;

import java.util.Collection;

/**
 * Service interface, defining methods which are required by all service classes.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public interface Service<T,ID> {

    /**
     * Saves the given object into the underlying repository.
     *
     * @param object    object to save
     * @return          true = successfully saved object, false = failed to save object
     */
    boolean save(T object);

    /**
     * Return the object with the given id;
     *
     * @param id        id of the object
     * @return          object with the given id
     */
    T get(ID id);

    /**
     * Returns all objects of the underlying repository
     *
     * @return          collection with all objects
     */
    Collection<T> getAll();

    /**
     * Delete object with the given id from the underlying repository.
     *
     * @param id        id of the object to delete
     * @return          true = successfully deleted object, false = couldn't delete this object
     */
    boolean delete(ID id);
}
