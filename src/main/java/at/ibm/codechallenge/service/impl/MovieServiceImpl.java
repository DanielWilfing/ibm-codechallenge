package at.ibm.codechallenge.service.impl;

import at.ibm.codechallenge.domainobjects.Movie;
import at.ibm.codechallenge.persistence.Repository;
import at.ibm.codechallenge.service.MovieService;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Implementation of the movie service.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public class MovieServiceImpl implements MovieService {

    private Repository<Movie,String> repository;

    public boolean save(Movie object) {

        if(object.getId() == null){
            return repository.save(null,object);
        }

        if(repository.get(object.getId()) == null){
            return repository.save(object.getId(),object);
        }

        return repository.update(object.getId(),object);
    }

    public Movie get(String s) {
        return repository.get(s);
    }

    public Collection<Movie> getAll() {
        return repository.getAll();
    }

    public boolean delete(String s) {
        return repository.delete(s);
    }

    public Movie findByTitle(String title) {

        for(Movie m : repository.getAll()){
            if(m.getTitle().equals(title)){
                return m;
            }
        }

        return null;
    }

    public Collection<Movie> findByTitlePart(String titlePart) {

        Collection<Movie> movies = new ArrayList<Movie>();
        titlePart = titlePart.toLowerCase();
        for(Movie m : repository.getAll()){
            if(m.getTitle().toLowerCase().contains(titlePart)){
                movies.add(m);
            }
        }
        return movies;
    }

    public void setRepository(Repository<Movie, String> repository) {
        this.repository = repository;
    }
}
