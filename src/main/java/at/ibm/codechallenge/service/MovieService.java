package at.ibm.codechallenge.service;

import at.ibm.codechallenge.domainobjects.Movie;

import java.util.Collection;

/**
 * Service interface for movies.
 * Defines additional methods, specially for movies.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public interface MovieService extends Service<Movie,String> {

    /**
     * Searchs for the title of a movie and returns a movie-object, when found.
     *
     * @param title     title of the movie to search for
     * @return          movie-object with the required title
     */
    Movie findByTitle(String title);

    /**
     * Searchs for movies, which have the given string in the title.
     *
     * @param titlePart string, which is in the title of the found movies
     * @return          collection of movie-objectss
     */
    Collection<Movie> findByTitlePart(String titlePart);
}
