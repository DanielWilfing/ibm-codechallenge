package at.ibm.codechallenge.domainobjects.dto;

/**
 * Data Transfer Object for movies.
 * Does miss a lot of different fields, which are not required for the given task.
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public class MovieDTO {

    private String id;

    /**
     * required fields for the dto
     */
    private String title;
    private String locations;

    public MovieDTO() {
    }

    public MovieDTO(String id, String title, String locations) {
        this.id = id;
        this.title = title;
        this.locations = locations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieDTO movieDTO = (MovieDTO) o;

        if (id != null ? !id.equals(movieDTO.id) : movieDTO.id != null) return false;
        if (title != null ? !title.equals(movieDTO.title) : movieDTO.title != null) return false;
        return locations != null ? locations.equals(movieDTO.locations) : movieDTO.locations == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (locations != null ? locations.hashCode() : 0);
        return result;
    }
}
