package at.ibm.codechallenge.domainobjects.transformer;

import at.ibm.codechallenge.domainobjects.Movie;
import at.ibm.codechallenge.domainobjects.dto.MovieDTO;

/**
 * Implementation of a transformer for movies
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public class MovieTransformer implements Transformer<Movie,MovieDTO> {
    public MovieDTO to(Movie object) {
        return new MovieDTO(object.getId(),object.getTitle(),object.getLocations());
    }

    public Movie from(MovieDTO movieDTO) {
        Movie m = new Movie();
        m.setId(movieDTO.getId());
        m.setTitle(movieDTO.getTitle());
        m.setLocations(movieDTO.getLocations());
        return m;
    }
}
