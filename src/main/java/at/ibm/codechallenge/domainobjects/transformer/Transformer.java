package at.ibm.codechallenge.domainobjects.transformer;

/**
 * Transformer interface, defining methods to transform objects into DTOs (or any other object).
 *
 * @author Daniel Wilfing - 24.9.2017.
 */
public interface Transformer<T,DTO> {

    /**
     * Transforms object into a DTO.
     *
     * @param object    object to transform
     * @return          new DTO object using data of the object
     */
    DTO to(T object);

    /**
     * Transform DTO into an object.
     *
     * @param dto       DTO to transform
     * @return          new object using data of the DTO
     */
    T from(DTO dto);
}
