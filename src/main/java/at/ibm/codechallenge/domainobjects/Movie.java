package at.ibm.codechallenge.domainobjects;

import java.io.Serializable;

/**
 * Information regarding movies made in San Francisco.
 *
 * @author Daniel Wilfing - 23.9.2017.
 */
public class Movie implements Serializable {

    private String id;

    /**
     * Fields in the movie json file
     */
    private String title;
    private String release_year;
    private String locations;
    private String fun_facts;
    private String production_company;
    private String distributor;
    private String director;
    private String writer;
    private String actor_1;
    private String actor_2;
    private String actor_3;

    public Movie() {
    }

    public Movie(String title, String release_year, String locations, String fun_facts, String production_company, String distributor, String director, String writer, String actor_1, String actor_2, String actor_3) {
        this.title = title;
        this.release_year = release_year;
        this.locations = locations;
        this.fun_facts = fun_facts;
        this.production_company = production_company;
        this.distributor = distributor;
        this.director = director;
        this.writer = writer;
        this.actor_1 = actor_1;
        this.actor_2 = actor_2;
        this.actor_3 = actor_3;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getFun_facts() {
        return fun_facts;
    }

    public void setFun_facts(String fun_facts) {
        this.fun_facts = fun_facts;
    }

    public String getProduction_company() {
        return production_company;
    }

    public void setProduction_company(String production_company) {
        this.production_company = production_company;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getActor_1() {
        return actor_1;
    }

    public void setActor_1(String actor_1) {
        this.actor_1 = actor_1;
    }

    public String getActor_2() {
        return actor_2;
    }

    public void setActor_2(String actor_2) {
        this.actor_2 = actor_2;
    }

    public String getActor_3() {
        return actor_3;
    }

    public void setActor_3(String actor_3) {
        this.actor_3 = actor_3;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (id != null ? !id.equals(movie.id) : movie.id != null) return false;
        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;
        if (release_year != null ? !release_year.equals(movie.release_year) : movie.release_year != null) return false;
        if (locations != null ? !locations.equals(movie.locations) : movie.locations != null) return false;
        if (fun_facts != null ? !fun_facts.equals(movie.fun_facts) : movie.fun_facts != null) return false;
        if (production_company != null ? !production_company.equals(movie.production_company) : movie.production_company != null)
            return false;
        if (distributor != null ? !distributor.equals(movie.distributor) : movie.distributor != null) return false;
        if (director != null ? !director.equals(movie.director) : movie.director != null) return false;
        if (writer != null ? !writer.equals(movie.writer) : movie.writer != null) return false;
        if (actor_1 != null ? !actor_1.equals(movie.actor_1) : movie.actor_1 != null) return false;
        if (actor_2 != null ? !actor_2.equals(movie.actor_2) : movie.actor_2 != null) return false;
        if (actor_3 != null ? !actor_3.equals(movie.actor_3) : movie.actor_3 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (release_year != null ? release_year.hashCode() : 0);
        result = 31 * result + (locations != null ? locations.hashCode() : 0);
        result = 31 * result + (fun_facts != null ? fun_facts.hashCode() : 0);
        result = 31 * result + (production_company != null ? production_company.hashCode() : 0);
        result = 31 * result + (distributor != null ? distributor.hashCode() : 0);
        result = 31 * result + (director != null ? director.hashCode() : 0);
        result = 31 * result + (writer != null ? writer.hashCode() : 0);
        result = 31 * result + (actor_1 != null ? actor_1.hashCode() : 0);
        result = 31 * result + (actor_2 != null ? actor_2.hashCode() : 0);
        result = 31 * result + (actor_3 != null ? actor_3.hashCode() : 0);
        return result;
    }
}
