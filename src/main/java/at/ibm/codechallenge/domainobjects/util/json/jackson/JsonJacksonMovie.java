package at.ibm.codechallenge.domainobjects.util.json.jackson;

import at.ibm.codechallenge.domainobjects.Movie;
import at.ibm.codechallenge.domainobjects.util.json.JsonService;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.http.HttpServlet;
import java.io.IOException;

/**
 * Uses jackson library to create/read json files.
 * Can load and create json files as well.
 *
 * @author Daniel Wilfing - 23.9.2017.
 */
public class JsonJacksonMovie extends HttpServlet implements JsonService<Movie[]> {

    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(JsonJacksonMovie.class);

    private ObjectMapper mapper = new ObjectMapper();

    public String writeObjectToJsonString(Movie[] object) throws IOException {
        return mapper.writeValueAsString(object);
    }

    public Movie[] readObjectFromJsonString(String json) throws IOException {
        return mapper.readValue(json,Movie[].class);
    }
}
