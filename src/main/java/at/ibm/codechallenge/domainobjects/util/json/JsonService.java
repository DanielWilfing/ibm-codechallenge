package at.ibm.codechallenge.domainobjects.util.json;

import java.io.IOException;

/**
 * Interface to read and write T objects to json.
 *
 * @author Daniel Wilfing - 23.9.2017.
 */
public interface JsonService<T> {

    /**
     * Transforms a object into an json string and returns it.
     *
     * @param object        object to transform into a string
     * @return              json-string of the object
     */
    String writeObjectToJsonString(T object) throws IOException;

    /**
     * Transforms a json String into an object returns it.
     *
     * @param json          json-string to transform into an object
     * @return              object created from the json-string
     */
    T readObjectFromJsonString(String json) throws IOException;

}
