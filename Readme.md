Movie IBM Challenge
===================


The REST-Interface allows different functionality. First, the basic functionality (Save, Update, Delete, Get) has been implemented. The Url for these functions is available at:
**https://ibmcodechallenge.herokuapp.com/movie**

>- SAVE: Send a POST to **https://ibmcodechallenge.herokuapp.com/movie**  containing title and location information. The id is not required. (e.g. {"title":"Title of my new movie","locations":"Pretty cool location"})
>- UPDATE: Send a PUT to **https://ibmcodechallenge.herokuapp.com/movie/ID**, where the ID is the ID of the object to update.
>- DELETE: Send a DELETE to **https://ibmcodechallenge.herokuapp.com/movie/ID**, where the ID is the ID of the object to delete.
>- GET: Send a GET to **https://ibmcodechallenge.herokuapp.com/movie**. This will load all available movie DTOs. You can also GET a single movie object, if you add the ID (similar to UPDATE and DELETE).

Additonally, filtering has been added as well. This functionality can be used by sending a GET to the following url: **https://ibmcodechallenge.herokuapp.com/movie/filter/STRING**.  The service will search through every title and return movies, which contain the given STRING in their title.

----------


Implementation
-------------

The implementation has been done the same way, in which I would've handled a bigger project. The implementation uses different layers (repository, service, external) and interfaces, making it possible to add new implementations. Configuration of the layers and classes is done using Spring Beans.

The JSON data is kept in a bean as well. This means that changes made to the data will be reset, if the server is restarted. For the given task (mainly being about REST), this was good enough. If I had more time, I would've set up a simple database and stored the data in there, instead of keeping it in the memory for the entire time.
